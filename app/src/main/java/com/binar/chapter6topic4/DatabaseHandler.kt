package com.binar.chapter6topic4

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper

class DatabaseHandler(context: Context) :
    SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    companion object {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "StudentDatabase"
        private val TABLE_CONTACTS = "StudentTable"
        private val KEY_ID = "id"
        private val KEY_NAME = "name"
        private val KEY_EMAIL = "email"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        //membuat table dengan fields
        val CREATE_CONTACTS_TABLE = ("CREATE TABLE " + TABLE_CONTACTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_EMAIL + " TEXT" + ")")
        db?.execSQL(CREATE_CONTACTS_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS)
        onCreate(db)
    }

    //method untuk menambahkan data
    fun addStudent(st: Student): Long {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_ID, st.userId)    // Id Student
        contentValues.put(KEY_NAME, st.userName) // Nama Student
        contentValues.put(KEY_EMAIL, st.userEmail) // Email Studient
        // Memasukkan Baris(Row)
        val success = db.insert(TABLE_CONTACTS, null, contentValues)
        //Argumen kedua adalah String yang berisi nullColumnHack
        db.close() // Menutup koneksi ke database
        return success
    }

    //method untuk membaca data
    fun viewStudent(): List<Student> {
        val stList: ArrayList<Student> = ArrayList()
        val selectQuery = "SELECT  * FROM $TABLE_CONTACTS"
        val db = this.readableDatabase
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery(selectQuery, null)
        } catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }
        var userId: Int
        var userName: String
        var userEmail: String
        if (cursor.moveToFirst()) {
            do {
                userId = cursor.getInt(cursor.getColumnIndex("id"))
                userName = cursor.getString(cursor.getColumnIndex("name"))
                userEmail = cursor.getString(cursor.getColumnIndex("email"))
                val st = Student(userId = userId, userName = userName, userEmail = userEmail)
                stList.add(st)
            } while (cursor.moveToNext())
        }
        return stList
    }

    //method untuk update data
    fun updateStudent(st: Student): Int {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_ID, st.userId)
        contentValues.put(KEY_NAME, st.userName) // Nama Student
        contentValues.put(KEY_EMAIL, st.userEmail) // Email Student

        // Update Baris
        val success = db.update(TABLE_CONTACTS, contentValues, "id=" + st.userId, null)
        //Argumen kedua adalah String yang berisi nullColumnHack
        db.close() // Menutup koneksi ke database
        return success
    }

    //method untuk menghapus data
    fun deleteStudent(st: Student): Int {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_ID, st.userId) // Id Student
        // Menghapus Baris
        val success = db.delete(TABLE_CONTACTS, "id=" + st.userId, null)
        //Argumen kedua adalah String yang berisi nullColumnHack
        db.close() // Menutup koneksi ke database
        return success
    }
}
