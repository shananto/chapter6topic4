package com.binar.chapter6topic4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ListAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    //method untuk menyimpan data ke dalam database
    fun saveRecord(view: View) {
        val id = etMainId.text.toString()
        val name = etMainName.text.toString()
        val email = etMainEmail.text.toString()
        val databaseHandler = DatabaseHandler(this)
        if (id.trim() != "" && name.trim() != "" && email.trim() != "") {
            val status =
                databaseHandler.addStudent(Student(Integer.parseInt(id), name, email))
            if (status > -1) {
                Toast.makeText(applicationContext, "Data Disimpan", Toast.LENGTH_LONG).show()
                etMainId.text.clear()
                etMainName.text.clear()
                etMainEmail.text.clear()
            }
        } else {
            Toast.makeText(
                applicationContext,
                "id atau nama atau email tidak boleh kosong",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    //method untuk menampilkan data dari databse
    fun viewRecord(view: View) {
        // Membuat instance dari class DatabaseHandler
        val databaseHandler = DatabaseHandler(this)
        //Memanggil method viewStudent dari class DatabaseHandler untuk membaca record
        val st: List<Student> = databaseHandler.viewStudent()
        val stArrayId = Array(st.size) { "0" }
        val stArrayName = Array(st.size) { "null" }
        val stArrayEmail = Array(st.size) { "null" }
        var index = 0
        for (e in st) {
            stArrayId[index] = e.userId.toString()
            stArrayName[index] = e.userName
            stArrayEmail[index] = e.userEmail
            index++
        }
        //Membuat ArrayAdapter custom
        val myListAdapter = ListAdapter(this, stArrayId, stArrayName, stArrayEmail)
        listView.adapter = myListAdapter
    }

    //method untuk update data berdasarkan id user
    fun updateRecord(view: View) {
        val dialogBuilder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.update_dialog, null)
        dialogBuilder.setView(dialogView)

        val uptId = dialogView.findViewById(R.id.updateId) as EditText
        val uptName = dialogView.findViewById(R.id.updateName) as EditText
        val uptEmail = dialogView.findViewById(R.id.updateEmail) as EditText

        dialogBuilder.setTitle("Update Data")
        dialogBuilder.setMessage("Masukkan Data Di bawah ini")
        dialogBuilder.setPositiveButton("Update") { _, _ ->

            val updateId = uptId.text.toString()
            val updateName = uptName.text.toString()
            val updateEmail = uptEmail.text.toString()
            //membuat instance dari class DatabaseHandler
            val databaseHandler: DatabaseHandler = DatabaseHandler(this)
            if (updateId.trim() != "" && updateName.trim() != "" && updateEmail.trim() != "") {
                //memanggil method updateStudent dari class DatabaseHelper untuk update data
                val status = databaseHandler.updateStudent(
                    Student(
                        Integer.parseInt(updateId),
                        updateName,
                        updateEmail
                    )
                )
                if (status > -1) {
                    Toast.makeText(applicationContext, "Data Diperbarui", Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(
                    applicationContext,
                    "id atau nama atau email tidak boleh kosong",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
        dialogBuilder.setNegativeButton("Cancel") { dialog, which ->
            //pass
        }
        val alertUpdate = dialogBuilder.create()
        alertUpdate.show()
    }

    //method untuk menghapus data sesuai dengan id
    fun deleteRecord(view: View) {
        //membuat AlertDialog untuk mengambil user id
        val dialogBuilder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.delete_dialog, null)
        dialogBuilder.setView(dialogView)

        val dltId = dialogView.findViewById(R.id.deleteId) as EditText
        dialogBuilder.setTitle("Hapus Data")
        dialogBuilder.setMessage("Masukkan id di bawah ini")
        dialogBuilder.setPositiveButton("Delete") { _, _ ->

            val deleteId = dltId.text.toString()
            //membuat instance dari class DatabaseHandler
            val databaseHandler = DatabaseHandler(this)
            if (deleteId.trim() != "") {
                //memanggil method deleteStudent dari class DatabaseHandler untuk menghapus data
                val status = databaseHandler.deleteStudent(
                    Student(
                        Integer.parseInt(deleteId),
                        "",
                        ""
                    )
                )
                if (status > -1) {
                    Toast.makeText(applicationContext, "Data Dihapus", Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(
                    applicationContext,
                    "id atau nama atau email tidak boleh kosong",
                    Toast.LENGTH_LONG
                ).show()
            }

        }
        dialogBuilder.setNegativeButton("Cancel") { _, _ ->
            //pass
        }
        val alertDelete = dialogBuilder.create()
        alertDelete.show()
    }

}
